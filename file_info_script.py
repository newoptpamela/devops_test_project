import os
from datetime import datetime

directory = "/tmp/test/"
result_dir = "/usr/share/nginx/html"
result_file = os.path.join(result_dir, "index.txt")

if not os.path.exists(directory):
    os.makedirs(directory)

if not os.path.exists(result_dir):
    os.makedirs(result_dir)

with open(result_file, "w") as output:
    files = os.listdir(directory)
    if not files:
        output.write("Файлы отсутствуют\n")
    else:
        for filename in files:
            file_path = os.path.join(directory, filename)
            if os.path.isfile(file_path):
                stat_info = os.stat(file_path)
                with open(file_path, "r") as f:
                    last_line = f.readlines()[-1].strip() if f.readlines() else ""
                output.write(f"{filename}:{datetime.fromtimestamp(stat_info.st_ctime)}:"
                             f"{datetime.fromtimestamp(stat_info.st_atime)}:{last_line}\n")

